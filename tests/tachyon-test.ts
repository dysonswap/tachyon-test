import * as anchor from "@coral-xyz/anchor";
import { Program } from "@coral-xyz/anchor";
import { TachyonTest } from "../target/types/tachyon_test";
import {
  decimalJsToRustDecimalBytes,
  getFunctions,
  rustDecimalBytesToDecimalJs,
  TachyonClient
} from "@dysonswap/tachyon";
import Decimal from "decimal.js";
import * as borsh from "borsh";
import {PublicKey} from "@solana/web3.js";
import * as fs from "fs";

describe("tachyon-test", () => {
  Decimal.set({ toExpPos: 1000, toExpNeg: -1000 })
  Decimal.set({ precision: 28 })

  let provider = anchor.AnchorProvider.env()
  anchor.setProvider(provider);

  const program = anchor.workspace.TachyonTest as Program<TachyonTest>;

  const tachyonClient = new TachyonClient(provider, new PublicKey("tachANmkv5KXR1hSZKoVJ2s5wKrfdgFgb3638k6CvKQ"))

  let baselineTachyonCU = 0
  let baselineNormalCU = 0

  let sampleSize = 1000

  it("baselineTachyon", async () => {
    const [tachyonFunctions, tachyonFunctionsPda] = await getFunctions(tachyonClient.program)

    let tx = await program.methods.baselineTachyon().accounts({
      user: provider.wallet.publicKey,
      functions: tachyonFunctionsPda,
      exp: tachyonFunctions.exp,
      ln: tachyonFunctions.ln,
    }).rpc()

    await provider.connection.confirmTransaction(tx, "confirmed");
    let ctx = await provider.connection.getTransaction(tx, {commitment: "confirmed",});

    baselineTachyonCU = ctx.meta.computeUnitsConsumed
    console.log("baselineTachyon CU", baselineTachyonCU)
  });

  it("tachyonExp", async () => {
    let domainStart = new Decimal(-11)
    let domainEnd = new Decimal(11)

    let xs = [...Array(sampleSize).keys()].map((x) => Decimal.random().mul(domainEnd.sub(domainStart)).add(domainStart))
    let results = await Promise.all(xs.map(async (x) => await tachyonExp(x)))

    let logger = fs.createWriteStream('tachyonExp.csv', { flags: 'a' })
    results.forEach((r) => logger.write(printResult(r as RunResult) + "\n"))
    logger.close()
  });

  it("tachyonLn", async () => {
    let domainStart = new Decimal(0.001)
    let domainEnd = new Decimal(1)

    let xs = [...Array(sampleSize).keys()].map((x) => Decimal.random().mul(domainEnd.sub(domainStart)).add(domainStart))
    let results = await Promise.all(xs.map(async (x) => await tachyonLn(x)))

    let logger = fs.createWriteStream('tachyonLn.csv', { flags: 'a' })
    results.forEach((r) => logger.write(printResult(r as RunResult) + "\n"))
    logger.close()
  });

  it("tachyonPow", async () => {
    let domainStart = new Decimal(0.001)
    let domainEnd = new Decimal(1)

    let xs = [...Array(sampleSize).keys()].map((x) => Decimal.random().mul(domainEnd.sub(domainStart)).add(domainStart))
    let results = await Promise.all(xs.map(async (x) => await tachyonPow(x, x)))

    let logger = fs.createWriteStream('tachyonPow.csv', { flags: 'a' })
    results.forEach((r) => logger.write(printResult(r as RunResult) + "\n"))
    logger.close()
  });

  it("baselineNormal", async () => {
    let tx = await program.methods.baselineNormal().accounts({
      user: provider.wallet.publicKey,
    }).rpc()

    await provider.connection.confirmTransaction(tx, "confirmed");
    let ctx = await provider.connection.getTransaction(tx, {commitment: "confirmed",});

    baselineNormalCU = ctx.meta.computeUnitsConsumed
    console.log("baselineNormal CU", baselineNormalCU)
  });

  it("normalExp", async () => {
    let domainStart = new Decimal(-11)
    let domainEnd = new Decimal(11)

    let xs = [...Array(sampleSize).keys()].map((x) => Decimal.random().mul(domainEnd.sub(domainStart)).add(domainStart))
    let results = await Promise.all(xs.map(async (x) => await normalExp(x)))

    let logger = fs.createWriteStream('normalExp.csv', { flags: 'a' })
    results.forEach((r) => logger.write(printResult(r as RunResult) + "\n"))
    logger.close()
  });

  it("normalLn", async () => {
    let domainStart = new Decimal(0.001)
    let domainEnd = new Decimal(1)

    let xs = [...Array(sampleSize).keys()].map((x) => Decimal.random().mul(domainEnd.sub(domainStart)).add(domainStart))
    let results = await Promise.all(xs.map(async (x) => await normalLn(x)))

    let logger = fs.createWriteStream('normalLn.csv', { flags: 'a' })
    results.forEach((r) => logger.write(printResult(r as RunResult) + "\n"))
    logger.close()
  });

  it("normalPow", async () => {
    let domainStart = new Decimal(0.001)
    let domainEnd = new Decimal(1)

    let xs = [...Array(sampleSize).keys()].map((x) => Decimal.random().mul(domainEnd.sub(domainStart)).add(domainStart))
    let results = await Promise.all(xs.map(async (x) => await normalPow(x, x)))

    let logger = fs.createWriteStream('normalPow.csv', { flags: 'a' })
    results.forEach((r) => logger.write(printResult(r as RunResult) + "\n"))
    logger.close()
  });

  type RunResult = {
    input: Decimal;
    output: Decimal;
    expected: Decimal;
    absoluteError: Decimal;
    relativeError: Decimal;
    cu: Decimal;
  }

  const printResult = (r: RunResult) => {
    return r.absoluteError + "," + r.relativeError + "," + r.cu
  }

  const tachyonExp = async (x: Decimal) => {
    const [tachyonFunctions, tachyonFunctionsPda] = await getFunctions(tachyonClient.program)

    let tx = await program.methods.tachyonExp(decimalJsToRustDecimalBytes(x)).accounts({
      user: provider.wallet.publicKey,
      functions: tachyonFunctionsPda,
      exp: tachyonFunctions.exp,
      ln: tachyonFunctions.ln,
    }).rpc()

    await provider.connection.confirmTransaction(tx, "confirmed");
    let ctx = await provider.connection.getTransaction(tx, {commitment: "confirmed",});

    let result = getDecimal(ctx)
    let expected = x.exp()

    let cu = ctx.meta.computeUnitsConsumed - baselineTachyonCU

    return {
      input: x,
      output: result,
      expected: expected,
      absoluteError: absoluteError(expected, result),
      relativeError: relativeError(expected, result),
      cu: cu,
    }
  }

  const tachyonLn = async (x: Decimal) => {
    const [tachyonFunctions, tachyonFunctionsPda] = await getFunctions(tachyonClient.program)

    let tx = await program.methods.tachyonLn(decimalJsToRustDecimalBytes(x)).accounts({
      user: provider.wallet.publicKey,
      functions: tachyonFunctionsPda,
      exp: tachyonFunctions.exp,
      ln: tachyonFunctions.ln,
    }).rpc()

    await provider.connection.confirmTransaction(tx, "confirmed");
    let ctx = await provider.connection.getTransaction(tx, {commitment: "confirmed",});

    let result = getDecimal(ctx)
    let expected = x.ln()

    let cu = ctx.meta.computeUnitsConsumed - baselineTachyonCU

    return {
      input: x,
      output: result,
      expected: expected,
      absoluteError: absoluteError(expected, result),
      relativeError: relativeError(expected, result),
      cu: cu,
    }
  }

  const tachyonPow = async (x: Decimal, pow: Decimal) => {
    const [tachyonFunctions, tachyonFunctionsPda] = await getFunctions(tachyonClient.program)

    let tx = await program.methods.tachyonPow(decimalJsToRustDecimalBytes(x), decimalJsToRustDecimalBytes(pow)).accounts({
      user: provider.wallet.publicKey,
      functions: tachyonFunctionsPda,
      exp: tachyonFunctions.exp,
      ln: tachyonFunctions.ln,
    }).rpc()

    await provider.connection.confirmTransaction(tx, "confirmed");
    let ctx = await provider.connection.getTransaction(tx, {commitment: "confirmed",});

    let result = getDecimal(ctx)
    let expected = x.pow(pow)

    let cu = ctx.meta.computeUnitsConsumed - baselineTachyonCU

    return {
      input: x,
      output: result,
      expected: expected,
      absoluteError: absoluteError(expected, result),
      relativeError: relativeError(expected, result),
      cu: cu,
    }
  }

  const normalExp = async (x: Decimal) => {
    const [tachyonFunctions, tachyonFunctionsPda] = await getFunctions(tachyonClient.program)

    let tx = await program.methods.normalExp(decimalJsToRustDecimalBytes(x)).accounts({
      user: provider.wallet.publicKey,
      functions: tachyonFunctionsPda,
      exp: tachyonFunctions.exp,
      ln: tachyonFunctions.ln,
    }).rpc()

    await provider.connection.confirmTransaction(tx, "confirmed");
    let ctx = await provider.connection.getTransaction(tx, {commitment: "confirmed",});

    let result = getDecimal(ctx)
    let expected = x.exp()

    let cu = ctx.meta.computeUnitsConsumed - baselineNormalCU

    return {
      input: x,
      output: result,
      expected: expected,
      absoluteError: absoluteError(expected, result),
      relativeError: relativeError(expected, result),
      cu: cu,
    }
  }

  const normalLn = async (x: Decimal) => {
    const [tachyonFunctions, tachyonFunctionsPda] = await getFunctions(tachyonClient.program)

    let tx = await program.methods.normalLn(decimalJsToRustDecimalBytes(x)).accounts({
      user: provider.wallet.publicKey,
      functions: tachyonFunctionsPda,
      exp: tachyonFunctions.exp,
      ln: tachyonFunctions.ln,
    }).rpc()

    await provider.connection.confirmTransaction(tx, "confirmed");
    let ctx = await provider.connection.getTransaction(tx, {commitment: "confirmed",});

    let result = getDecimal(ctx)
    let expected = x.ln()

    let cu = ctx.meta.computeUnitsConsumed - baselineNormalCU

    return {
      input: x,
      output: result,
      expected: expected,
      absoluteError: absoluteError(expected, result),
      relativeError: relativeError(expected, result),
      cu: cu,
    }
  }

  const normalPow = async (x: Decimal, pow: Decimal) => {
    const [tachyonFunctions, tachyonFunctionsPda] = await getFunctions(tachyonClient.program)

    let tx = await program.methods.normalPow(decimalJsToRustDecimalBytes(x), decimalJsToRustDecimalBytes(pow)).accounts({
      user: provider.wallet.publicKey,
      functions: tachyonFunctionsPda,
      exp: tachyonFunctions.exp,
      ln: tachyonFunctions.ln,
    }).rpc()

    await provider.connection.confirmTransaction(tx, "confirmed");
    let ctx = await provider.connection.getTransaction(tx, {commitment: "confirmed",});

    let result = getDecimal(ctx)
    let expected = x.pow(pow)

    let cu = ctx.meta.computeUnitsConsumed - baselineNormalCU

    return {
      input: x,
      output: result,
      expected: expected,
      absoluteError: absoluteError(expected, result),
      relativeError: relativeError(expected, result),
      cu: cu,
    }
  }

});

const relativeError = (expected: Decimal, result: Decimal): Decimal => {
  return (expected.sub(result).abs()).div(expected.abs())
}

const absoluteError = (expected: Decimal, result: Decimal): Decimal => {
  return expected.sub(result).abs()
}

const getReturnLog = (confirmedTransaction) => {
  const prefix = "Program return: ";
  let log = confirmedTransaction.meta.logMessages.find((log) =>
      log.startsWith(prefix)
  );
  log = log.slice(prefix.length);
  const [key, data] = log.split(" ", 2);
  const buffer = Buffer.from(data, "base64");
  return [key, data, buffer];
};

const getDecimal = (ctx): Decimal => {
  const [key, data, buffer] = getReturnLog(ctx);

  const reader = new borsh.BinaryReader(buffer);
  let array = reader.readFixedArray(16);

  return rustDecimalBytesToDecimalJs(array)
};
