# Tachyon Test Repo

Tachyon is a tool for evaluating transcendental functions on Solana. It uses pre-populated lookup tables along with interpolation for function evaluation.

This repo serves two purposes:
* test the performance of Tachyon vs. rust_decimal
* illustrate how to use Tachyon in your own project

## Performance

Compute units and error were compared for three functions:
* Exponential
* Natural logarithm
* Power function

These three functions were compared using:
* Tachyon with linear interpolation
* Tachyon with quadratic interpolation
* rust_decimal (with default parameters)

Below are a few charts illustrating the main findings:

</br><img src="images/compute.png" alt="avg_compute" width="600"/></br>

On average, Tachyon uses far less compute units than rust_decimal.

</br><img src="images/max_compute.png" alt="max_compute" width="600"/></br>

For Solana programs, the average CU above is often less important than the maximum CU used, since transactions have a maximum of 1.4M CU, and need to be reliably executed. Tachyon uses nearly constant CU, regardless of function input.
Here we seen an improvement of 8-23x for linear interpolation, and 6-15x for quadratic interpolation. 

</br><img src="images/absolute_error.png" alt="max_compute" width="600"/></br>

Absolute error is the difference between the actual result and the expected result.
rust_decimal is surprisingly inaccurate when it comes to exp calculations.
For error rates which are too low to appear on the plot above, they can be seen in the last image below.

</br><img src="images/relative_error.png" alt="relative_error" width="600"/></br>

Relative error is absolute error divided by the expected result. This is a little more helpful in understanding the magnitude of error, since absolute error is highly dependent on the function and function inputs (high or low numbers).
The linear interpolation is not as accurate as either quadratic or rust_decimal. Based on the data above, I would recommend using the quadratic interpolation for most use-cases.

</br><img src="images/data.png" alt="data" width="500"/></br>

The data above was generated with a random sample of 1000 numbers. Running `anchor test` will write the errors/CU to text files.

## Using Tachyon

In this repo, Tachyon is used by passing a few Solana accounts into the instruction, while also having imported the Tachyon program directly. This example does not use CPI calls.

Accounts passed:
* Functions account (has a list of all lookup tables)
* Exp lookup table account
* Ln lookup table account

The account struct (used in all instructions here) looks like this:

```
#[derive(Accounts)]
pub struct TachyonAccounts<'info> {
    pub user: Signer<'info>,
    pub functions: Box<Account<'info, Functions>>,
    #[account(
        constraint = exp.key() == functions.exp
    )]
    pub exp: AccountLoader<'info, FunctionData>,
    #[account(
        constraint = ln.key() == functions.ln
    )]
    pub ln: AccountLoader<'info, FunctionData>,
}
```

The `exp` and `ln` accounts are checked via a constraint that compares them to what is listed in the `functions` account.

The functions account is then later checked with:
```
let (tachyon_functions, _tachyon_functions_bump) = Pubkey::find_program_address(&[TACHYON_FUNCTIONS_SEED], &TACHYON_PROGRAM_ID);
if tachyon_functions != pubkey {
    return err!(error::ErrorCode::InvalidTachyonFunctionsAccount);
}
```

Once inside the instruction handler, a `TachyonCalculator` is constructed.

```
let exp = ctx.accounts.exp.load()?;
let ln = ctx.accounts.ln.load()?;

let calc = TachyonCalculator {
    exp: Some(&exp),
    ln: Some(&ln),
    log10: None,
    sin: None,
    cos: None,
};
```

From here, we can use the calculator for the function evaluations.
(rust_decimal input, rust_decimal output) 

`let y = calc.ln(my_input_decimal, Interpolation::Quadratic)?;`

For `exp` there is an additional argument: `saturating` (true/false)
A `saturating` value of true will return the highest value in the lookup table if the function input goes beyond the max value in the lookup table.
The max value for exp is ~66.5, and exp(66.5) is just about the biggest number that rust_decimal can hold: 79228162514264337593543950335. For most cases this should be fine.
Similarly, if a number below -66.5 is used, and saturating is true, it will return 1/79228162514264337593543950335.
If saturating is false, then it will simply throw an error when out of bounds.

## Local testing
When testing locally, instead of populating the lookup tables every time (which takes quite a while) it's faster to pull the accounts directly from mainnet, which can be done in the `Anchor.toml` file.

Here are the relevant sections:
```
[[test.genesis]]
address = "tachANmkv5KXR1hSZKoVJ2s5wKrfdgFgb3638k6CvKQ"
program = "tachyon.so"

# tachyon accounts from mainnet-beta
# functions
[[test.validator.clone]]
address = "59AQFUrM1XnWAF5bwnPda3qnFmg51YexDAUuzTq6hpii"

# exp, 200k points, 0 to 66.5 (though domain is -66.5 to 66.5 when using inverse for negative x)
[[test.validator.clone]]
address = "D1bM7VyXicYjWTxzJe48KVgUY1mVetfXcZqpocTX48oq"

# ln, 200k points, 0 to 1 (this domain is what was needed for Dyson Swap)
[[test.validator.clone]]
address = "Hfy15cSRgHqzPs9a3rHuPQPvM2ewXw75VySdMwFhG7Va"
```

Within this repo is the tachyon.so file, which can also be created by building the program from the Tachyon repo itself.


### Getting in touch
If there are any comments or questions, I can be reached on Twitter at @joebuild. DMs are open.
