
use anchor_lang::prelude::*;
use rust_decimal::Decimal;
use solana_program::pubkey;
use tachyon_math_library::calculator::TachyonCalculator;
use tachyon_math_library::state::{FunctionData, Functions};
use tachyon_math_library::state::Interpolation;
use num_traits::pow::Pow;

mod error;

declare_id!("ttxtBtYYE2nz86XLSr7Rtc5uXbyh5ZqFiGDvoxNpDXR");

pub const TACHYON_PROGRAM_ID: Pubkey = pubkey!("tachANmkv5KXR1hSZKoVJ2s5wKrfdgFgb3638k6CvKQ");
pub const TACHYON_FUNCTIONS_SEED: &[u8] = b"functions";

#[program]
pub mod tachyon_test {
    use rust_decimal::MathematicalOps;
    use super::*;

    pub fn baseline_tachyon(ctx: Context<TachyonAccounts>) -> Result<()> {
        check_functions_account(ctx.accounts.functions.key())?;

        let exp = ctx.accounts.exp.load()?;
        let ln = ctx.accounts.ln.load()?;

        let calc = TachyonCalculator {
            exp: Some(&exp),
            ln: Some(&ln),
            log10: None,
            sin: None,
            cos: None,
        };

        Ok(())
    }

    pub fn tachyon_exp(ctx: Context<TachyonAccounts>, x_raw: [u8; 16]) -> Result<[u8; 16]> {
        check_functions_account(ctx.accounts.functions.key())?;

        let exp = ctx.accounts.exp.load()?;
        let ln = ctx.accounts.ln.load()?;

        let calc = TachyonCalculator {
            exp: Some(&exp),
            ln: Some(&ln),
            log10: None,
            sin: None,
            cos: None,
        };

        let y = calc.exp(Decimal::deserialize(x_raw), Interpolation::Linear, true)?;

        Ok(y.serialize())
    }

    pub fn tachyon_ln(ctx: Context<TachyonAccounts>, x_raw: [u8; 16]) -> Result<[u8; 16]> {
        check_functions_account(ctx.accounts.functions.key())?;

        let exp = ctx.accounts.exp.load()?;
        let ln = ctx.accounts.ln.load()?;

        let calc = TachyonCalculator {
            exp: Some(&exp),
            ln: Some(&ln),
            log10: None,
            sin: None,
            cos: None,
        };

        let y = calc.ln(Decimal::deserialize(x_raw), Interpolation::Linear)?;

        Ok(y.serialize())
    }

    pub fn tachyon_pow(ctx: Context<TachyonAccounts>, x_raw: [u8; 16], pow_raw: [u8; 16]) -> Result<[u8; 16]> {
        check_functions_account(ctx.accounts.functions.key())?;

        let exp = ctx.accounts.exp.load()?;
        let ln = ctx.accounts.ln.load()?;

        let calc = TachyonCalculator {
            exp: Some(&exp),
            ln: Some(&ln),
            log10: None,
            sin: None,
            cos: None,
        };

        let y = calc.pow(Decimal::deserialize(x_raw), Decimal::deserialize(pow_raw), Interpolation::Linear, true)?;

        Ok(y.serialize())
    }

    pub fn baseline_normal(ctx: Context<NormalAccounts>) -> Result<()> {
        Ok(())
    }

    pub fn normal_exp(ctx: Context<NormalAccounts>, x_raw: [u8; 16]) -> Result<[u8; 16]> {
        Ok(Decimal::deserialize(x_raw).exp().serialize())
    }

    pub fn normal_ln(ctx: Context<NormalAccounts>, x_raw: [u8; 16]) -> Result<[u8; 16]> {
        Ok(Decimal::deserialize(x_raw).ln().serialize())
    }

    pub fn normal_pow(ctx: Context<NormalAccounts>, x_raw: [u8; 16], pow_raw: [u8; 16]) -> Result<[u8; 16]> {
        Ok(Decimal::deserialize(x_raw).pow(Decimal::deserialize(pow_raw)).serialize())
    }
}

fn check_functions_account(
    pubkey: Pubkey,
) -> Result<()> {
    let (tachyon_functions, _tachyon_functions_bump) = Pubkey::find_program_address(&[TACHYON_FUNCTIONS_SEED], &TACHYON_PROGRAM_ID);
    if tachyon_functions != pubkey {
        return err!(error::ErrorCode::InvalidTachyonFunctionsAccount);
    }

    Ok(())
}

#[derive(Accounts)]
pub struct TachyonAccounts<'info> {
    pub user: Signer<'info>,
    pub functions: Box<Account<'info, Functions>>,
    #[account(
        constraint = exp.key() == functions.exp
    )]
    pub exp: AccountLoader<'info, FunctionData>,
    #[account(
        constraint = ln.key() == functions.ln
    )]
    pub ln: AccountLoader<'info, FunctionData>,
}

#[derive(Accounts)]
pub struct NormalAccounts<'info> {
    pub user: Signer<'info>,
}
